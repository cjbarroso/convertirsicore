fp = open("sicore.txt")
lineas = fp.readlines()
lineascor = [e.strip() for e in lineas]

fout = open("salida.txt", "w")


for e in range(0, len(lineascor)):
    retenido = float(lineascor[e][71:88].replace(",", "."))
    signo = retenido > 0
    alicuota = float(lineascor[e][66:71].replace(",", "."))
    retenido = "%012.2f" % (retenido * alicuota)
    alicuota = "%05.2f" % alicuota
    paso = "%05d" % e
    tip_comp = ("1" if signo else "2")
    ori_comp = "1"
    fecha = lineascor[e][2:12]
    nfact = lineascor[e][16:24].zfill(12)
    cuit = lineascor[e][40:51]
    totmonto = float(lineascor[e][52:66].replace(",", "."))
    if not signo:
        totmonto = totmonto * -1
    monto = "%012.2f" % totmonto
    gob1 = "020"
    gob2 = "916"
    fout.write(f"{paso},{ori_comp},{tip_comp},{nfact},{cuit},{fecha},{monto},{alicuota},{retenido},{gob1},{gob2}\n")
